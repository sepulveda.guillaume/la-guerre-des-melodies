const express = require("express");
const router = express.Router();
const { postUser, deleteUser } = require("../controllers/user.controller");

// POST user
router.post("/", postUser);

// DELETE user
router.delete("/:id", deleteUser);

module.exports = router;
