const express = require("express");
const router = express.Router();
const {
  getAllGames,
  getGameById,
  checkAnswer,
  postGame,
  deleteGame,
} = require("../controllers/game.controller");

// GET games
router.get("/", getAllGames);

// GET game
router.get("/:id", getGameById);

// POST game
router.post("/checkAnswer", checkAnswer);

// POST new game
router.post("/", postGame);

// DELETE game by id
router.delete("/:id", deleteGame);

module.exports = router;
