const router = require("express").Router();
const songRoutes = require("./song.routes");
const userRoutes = require("./user.routes");
const gameParamRoutes = require("./gameParam.routes");
const gameRoutes = require("./game.routes");

router.use("/songs", songRoutes);
router.use("/users", userRoutes);
router.use("/gameParams", gameParamRoutes);
router.use("/games", gameRoutes);

module.exports = router;
