const express = require("express");
const router = express.Router();
const {
  getAllSongs,
  getSongById,
  postSong,
  putSong,
  deleteSong,
} = require("../controllers/song.controller");
const upload = require("../middlewares/upload");

// GET songs
router.get("/", getAllSongs);

// GET song
router.get("/:id", getSongById);

// POST new song
router.post("/", upload, postSong);

// PUT song by id
router.put("/:id", putSong);

// DELETE song by id
router.delete("/:id", deleteSong);

module.exports = router;
