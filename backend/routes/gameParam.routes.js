const express = require("express");
const router = express.Router();
const { getGameParams } = require("../controllers/gameParam.controller");

// GET game params
router.get("/", getGameParams);

module.exports = router;
