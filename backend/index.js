const express = require("express");
const app = express();
const cors = require("cors");
const mongoose = require("mongoose");
const router = require("./routes/index");
const { createServer } = require("http");
const port = process.env.PORT || 3000;
const websocket = require("./utils/websocket");

require("dotenv").config();

const server = createServer(app);

mongoose
  .connect(process.env.MANGOOSE_CONNECTION)
  .then(() => console.log("Connexion à MongoDB réussie !"))
  .catch(() => console.log("Connexion à MongoDB échouée !"));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/", router);

server.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
