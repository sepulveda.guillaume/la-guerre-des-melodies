const Song = require("../models/Song");
const logger = require("progress-estimator")();
const ytdl = require("ytdl-core");
const storage = require("../utils/firebase");
const {
  ref,
  uploadBytes,
  getDownloadURL,
  deleteObject,
} = require("firebase/storage");
const fs = require("fs");
const path = require("path");
const os = require("os");
const ffmpeg = require("fluent-ffmpeg");
const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;

ffmpeg.setFfmpegPath(ffmpegPath);

const uploadSongSpotifyToFirebaseStorage = async (
  urlDownload,
  audioFileName
) => {
  try {
    // Télécharge le fichier audio à partir de son URL
    const response = await fetch(urlDownload);
    const audioBlob = await response.blob(); // Convertit la réponse en blob

    await logger(audioBlob, `Downloading ${audioFileName}`);

    // Crée une référence Firebase Storage pour le fichier
    const audioRef = ref(storage, audioFileName);

    // Téléverse le fichier blob vers Firebase Storage
    await uploadBytes(audioRef, audioBlob);

    const downloadUrl = await getDownloadURL(audioRef);

    return downloadUrl;
  } catch (error) {
    console.error("Erreur lors du téléversement du fichier audio :", error);
    throw error;
  }
};

const uploadSongYoutubeToFirebaseStorage = async (
  urlDownload,
  audioFileName
) => {
  try {
    const tmpDir = os.tmpdir();

    const audioFilePathMP3 = path.join(tmpDir, `full_${audioFileName}`);

    console.log("Chemin du fichier temporaire full :", audioFilePathMP3);

    // Téléchargement de l'audio depuis YouTube avec ytdl-core
    const audioStream = ytdl(urlDownload, { quality: "highestaudio" });
    audioStream.pipe(fs.createWriteStream(audioFilePathMP3));

    // Attendre que le téléchargement soit terminé
    await new Promise((resolve, reject) => {
      audioStream.on("end", resolve);
      audioStream.on("error", reject);
    });

    console.log("Promise download :", audioStream);

    await logger(audioStream, `Downloading ${audioFileName}`);

    const audioFilePath = path.join(tmpDir, audioFileName);

    console.log("Chemin du fichier temporaire :", audioFilePath);

    // Démarrer la conversion avec ffmpeg pour couper l'audio à 29 secondes
    await new Promise((resolve, reject) => {
      ffmpeg(audioFilePathMP3)
        .setStartTime("00:00:00")
        .setDuration("00:00:29")
        .output(audioFilePath)
        .on("end", resolve)
        .on("error", reject)
        .run();
    });

    console.log("Fichier audio coupé :", audioFilePath);

    // Créer une référence Firebase Storage pour le fichier coupé
    const cutAudioRef = ref(storage, audioFileName);

    // Lire le contenu du fichier audio
    const audioData = fs.readFileSync(audioFilePath);

    console.log("Contenu du fichier audio :", audioData);

    // Créer un Blob avec le contenu du fichier et le type MIME approprié
    const audioBlob = new Blob([audioData], { type: "audio/mp3" });

    console.log("Blob audio :", audioBlob);

    // Téléverser le fichier coupé vers Firebase Storage avec le bon type MIME
    await uploadBytes(cutAudioRef, audioBlob);

    // Supprimer les fichiers locaux
    fs.unlinkSync(audioFilePath);
    fs.unlinkSync(audioFilePathMP3);

    // Récupérer l'URL de téléchargement du fichier coupé
    const downloadUrl = await getDownloadURL(cutAudioRef);

    return downloadUrl;
  } catch (error) {
    console.error("Erreur lors du téléversement du fichier audio :", error);
    throw error;
  }
};

const getAllSongs = async (req, res) => {
  try {
    const { theme } = req.query;
    let songs;
    if (theme) {
      songs = await Song.find({ themes: theme });
    } else {
      songs = await Song.find();
    }
    res.json(songs);
  } catch (error) {
    console.error("Error in getAllSongs:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const getSongById = async (req, res) => {
  try {
    const song = await Song.findById(req.params.id);

    if (!song) {
      return res.status(404).json({ error: "Song not found" });
    }
    res.json({ audioFilePath: song.audioFilePath });
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const postSong = async (req, res) => {
  try {
    const { title, artist, url, type, themes } = req.body;

    console.log(`Creating song:, ${title}, ${artist}, ${url}, ${type}, ${themes}`);

    const song = await Song.create({ title, artist, url, themes });

    let urlDownload = url;
    let audioPath;

    const audioFileName = `${song._id}.mp3`;

    if (type === "mp4") {
      audioPath = await uploadSongYoutubeToFirebaseStorage(
        urlDownload,
        audioFileName
      );
    }

    if (type === "mp3") {
      audioPath = await uploadSongSpotifyToFirebaseStorage(
        urlDownload,
        audioFileName
      );
    }

    song.audioFilePath = audioPath;

    await song.save();

    res.status(201).json(song);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const putSong = async (req, res) => {
  try {
    const song = await Song.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    if (!song) {
      return res.status(404).json({ error: "Song not found" });
    }
    res.json(song);
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const deleteSong = async (req, res) => {
  try {
    const song = await Song.findByIdAndDelete(req.params.id);
    if (!song) {
      return res.status(404).json({ error: "Song not found" });
    }

    const audioRef = ref(storage, `${song._id}.mp3`);
    await deleteObject(audioRef);

    res.json({ message: "Song deleted successfully" });
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
};

module.exports = {
  getAllSongs,
  getSongById,
  postSong,
  putSong,
  deleteSong,
};
