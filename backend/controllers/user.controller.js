const User = require("../models/User");

const postUser = async (req, res) => {
  try {
    const user = await User.create(req.body);
    res.status(201).json(user);
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const deleteUser = async (req, res) => {
  try {
    const user = await User.findByIdAndDelete(req.params.id);
    res.status(200).json(user);
  }
  catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
}

module.exports = {
  postUser,
  deleteUser
};
