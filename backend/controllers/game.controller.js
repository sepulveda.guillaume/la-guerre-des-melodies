const Game = require("../models/Game");
const Song = require("../models/Song");
const { distance } = require("fastest-levenshtein");

const getAllGames = async (req, res) => {
  try {
    const games = await Game.find();
    res.json(games);
  } catch (error) {
    console.error("Error in getAllGames:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const getGameById = async (req, res) => {
  try {
    const game = await Game.findById(req.params.id);
    if (!game) {
      return res.status(404).json({ error: "Game not found" });
    }
    res.json(game);
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const checkAnswer = async (req, res) => {
  try {
    const { title, artist, gameId, username } = req.body;

    const game = await Game.findById(gameId);

    if (!game) {
      return res.status(404).json({ error: "Game not found" });
    }

    // Récupérer la chanson actuelle en fonction de l'index
    const currentSongId = game.songsIds[game.currentSongIndex];
    const song = await Song.findById(currentSongId);

    // Supprimer les espaces et convertir en minuscules pour les comparaisons
    const formattedTitle = title.replace(/\s/g, "").toLowerCase();
    const formattedArtist = artist.replace(/\s/g, "").toLowerCase();
    const formattedSongTitle = song?.title.replace(/\s/g, "").toLowerCase();
    const formattedSongArtist = song?.artist.replace(/\s/g, "").toLowerCase();

    // Vérifier la réponse de l'utilisateur avec une tolérance d'erreur
    const titleDistance = distance(formattedSongTitle, formattedTitle);
    const artistDistance = distance(formattedSongArtist, formattedArtist);

    const maxAllowedDistance = 1; // Tolérance d'erreur

    const correctTitle = titleDistance <= maxAllowedDistance;
    const correctArtist = artistDistance <= maxAllowedDistance;

    let userScore = game.scores.get(username) || 0;

    // Mettre à jour les scores en fonction de la réponse
    if (correctTitle && correctArtist) {
      userScore += 3;
    } else if (correctTitle || correctArtist) {
      userScore += 1;
    }

    game.scores.set(username, userScore);

    // Passer à la prochaine chanson
    game.currentSongIndex++;

    // Vérifier si toutes les chansons ont été jouées
    if (game.currentSongIndex >= game.songsIds.length) {
      game.status = "finished";
    }

    // Sauvegarder les modifications du jeu
    await game.save();

    // Renvoyer la réponse au front-end
    res.json({
      userCorrectTitle: correctTitle,
      userCorrectArtist: correctArtist,
      correctTitle: song?.title,
      correctArtist: song?.artist,
      game,
    });
  } catch (error) {
    console.error("Error checking answer:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const postGame = async (req, res) => {
  try {
    const game = new Game(req.body);
    await game.save();
    res.status(201).json(game);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const deleteGame = async (req, res) => {
  try {
    const game = await Game.findByIdAndDelete(req.params.id);
    if (!game) {
      return res.status(404).json({ error: "Game not found" });
    }
    res.json({ message: "Song deleted successfully" });
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
};

module.exports = {
  getAllGames,
  getGameById,
  checkAnswer,
  postGame,
  deleteGame,
};
