const GameParam = require("../models/GameParam");

const getGameParams = async (req, res) => {
  try {
    const gameParams = await GameParam.find();
    res.status(200).json(gameParams);
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
};

module.exports = {
    getGameParams
};
