const mongoose = require("mongoose");

const gameParamSchema = new mongoose.Schema({
  duration: {
    type: Number,
    required: true,
  },
  numberOfSongs: {
    type: Number,
    required: true,
  },
});

const GameParam = mongoose.model("GameParam", gameParamSchema);

module.exports = GameParam;
