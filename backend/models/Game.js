const mongoose = require("mongoose");
const Song = require("../models/Song");

const gameSchema = new mongoose.Schema({
  players: {
    type: [String], // Array of player usernames
    required: true,
  },
  songsIds: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Song",
    },
  ],
  scores: {
    type: Map, // Map to store scores with player username as key
    of: Number, // Score as the value
    default: {}, // Default value is an empty object
  },
  currentSongIndex: {
    type: Number, // Index of the currently playing song in the 'songs' array
    default: 0, // Initially set to the first song
  },
  status: {
    type: String,
    default: "ongoing",
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

const Game = mongoose.model("Game", gameSchema);

module.exports = Game;
