const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  // stats: {
  //   totalGames: Number,
  //   totalWins: Number,
  //   totalLosses: Number
  // }
});

const User = mongoose.model("User", userSchema);

module.exports = User;
