const Ably = require("ably");
const axios = require("axios");
const Game = require("../models/Game");
const port = process.env.PORT || 3000;

const ably = new Ably.Realtime(process.env.ABLY_API_KEY);

const configUrl =
  process.env.NODE_ENV === "production"
    ? "https://la-guerre-des-melodies-api.vercel.app"
    : `http://localhost:${port}`;

ably.connection.once("connected", () => {
  console.log("Connected to Ably!");
});

let participants = [];

ably.channels.get("game").subscribe("joinLobby", (msg) => {
  const auth = msg.data;
  if (!participants.some((p) => p.id === auth.userId)) {
    participants.push({
      id: auth.userId,
      username: auth.username,
      score: 0,
      ready: false,
    });
  } else {
    const participant = participants.find((p) => p.id === auth.userId);
    participant.ready = false;
  }

  ably.channels.get("game").publish("updateParticipants", participants);
});

ably.channels.get("game").subscribe("leaveLobby", (msg) => {
  const userId = msg.data.userId;
  participants = participants.filter((p) => p.id !== userId);
  ably.channels.get("game").publish("updateParticipants", participants);
});

ably.channels.get("game").subscribe("toggleReady", (msg) => {
  const userId = msg.data.userId;
  const participant = participants.find((p) => p.id === userId);
  participant.ready = !participant.ready;
  ably.channels.get("game").publish("updateParticipants", participants);
});

ably.channels.get("game").subscribe("startGame", async (msg) => {
  try {
    // 1. Récupérer les paramètres du jeu depuis votre propre API
    const paramsResponse = await axios.get(`${configUrl}/gameParams`);
    const gameParams = paramsResponse.data[0];

    // 2. Extraire le thème du message s'il y en a un
    const theme = msg?.data;

    // 3. Récupérer toutes les chansons en fonction du thème spécifié dans le message
    const themeQueryParam = theme ? `?theme=${theme}` : '';
    const songsResponse = await axios.get(`${configUrl}/songs${themeQueryParam}`);
    const allSongs = songsResponse.data;

    // 4. Sélectionner les chansons de manière aléatoire en fonction du nombre de chansons requis par les paramètres du jeu
    const randomSongs = allSongs
      .sort(() => 0.5 - Math.random())
      .slice(0, gameParams.numberOfSongs);

    // 5. Créer une nouvelle instance de jeu avec les participants actuels et les chansons sélectionnées
    const game = new Game({
      players: participants.map((participant) => participant.username),
      songsIds: randomSongs.map((song) => song._id),
      scores: participants.reduce((acc, participant) => {
        acc[participant.username] = 0;
        return acc;
      }, {}),
      currentSongIndex: 0,
      status: "ongoing",
    });

    // 6. Sauvegarder le jeu dans la base de données
    await game.save();

    // 7. Diffuser un événement pour informer les clients que le jeu a commencé,
    // en incluant l'ID du jeu dans la diffusion
    ably.channels.get("game").publish("gameStarted", { game, gameParams });

    // 8. Réinitialiser la liste des participants pour le prochain jeu
    // participants = [];
  } catch (error) {
    console.error("Erreur lors du démarrage du jeu :", error);
  }
});

ably.channels.get("game").subscribe("disconnect", (msg) => {
  const userId = msg.data.userId;
  participants = participants.filter((p) => p.id !== userId);
  ably.channels.get("game").publish("updateParticipants", participants);

  participants = [];
});

process.on("exit", () => {
  ably.close();
});

module.exports = {
  ably,
  participants,
};
