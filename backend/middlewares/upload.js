const multer = require("multer");
const DIR = "./songs/";

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, DIR);
  },
  filename: (req, file, callback) => {
    const { originalname } = file;
    const nameWithoutExtension = originalname.split(".").slice(0, -1).join(".");
    const extension = originalname.split(".").pop();
    const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1e9)}`;
    const newFilename = `${nameWithoutExtension}-${uniqueSuffix}.${extension}`;

    callback(null, newFilename);
  },
});

const upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "video/mp4"
    ) {
      cb(null, true);
    } else {
      cb(null, false);
      return cb(new Error("Only .mp4 format allowed!"));
    }
  },
});

module.exports = upload.single("song");

