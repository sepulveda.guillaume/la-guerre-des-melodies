import { extendTheme } from "@chakra-ui/react";

const config = {
  initialColorMode: "dark",
  useSystemColorMode: false,
};

const theme = extendTheme({
  config,
  colors: {
    light: {
      bgMain: "#ffffff",
      bgReverse: "#141414",
      bgRed: "#e50914",
      bgRedHover: "#ff0000",
      bgWhite: "#ffffff",
      bgWhiteHover: "rgba(255, 255, 255, 0.75)",
      bgGreen: "#1DB954",
      bgGreenHover: "#1ed760",
      bgGray: "#cccccc",
      txtMain: "#ffffff",
      txtReverse: "#141414",
      txtGray: "#333333",
      txtGrayReverse: "#cccccc",
      txtRed: "#e50914",
      txtGreen: "#1DB954",
      txtGold: "#ffd700",
      txtSilver: "#c0c0c0",
      txtBronze: "#cd7f32",
      boxShadow: "rgba(0, 0, 0, 0.1)",
      modalOverlay: "#ffffffCC",
    },
    dark: {
      bgMain: "#141414",
      bgReverse: "#ffffff",
      bgRed: "#e50914",
      bgRedHover: "#ff0000",
      bgWhite: "#ffffff",
      bgWhiteHover: "rgba(255, 255, 255, 0.75)",
      bgGreen: "#1DB954",
      bgGreenHover: "#1ed760",
      bgGray: "#333333",
      txtMain: "#141414",
      txtReverse: "#ffffff",
      txtGray: "#cccccc",
      txtGrayReverse: "#333333",
      txtRed: "#e50914",
      txtGreen: "#1DB954",
      txtGold: "#ffd700",
      txtSilver: "#c0c0c0",
      txtBronze: "#cd7f32",
      boxShadow: "rgba(255, 255, 255, 0.1)",
      modalOverlay: "#141414CC",
    },
  },
  styles: {
    global: ({ colorMode }) => ({
      body: {
        bg: colorMode === "light" ? "light.bgMain" : "dark.bgMain",
      },
    }),
  },
});

export default theme;
