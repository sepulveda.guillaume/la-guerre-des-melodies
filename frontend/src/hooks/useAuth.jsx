import { createContext, useContext } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useLocalStorage } from "./useLocalStorage";

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [auth, setAuth] = useLocalStorage("auth", null);
  const navigate = useNavigate();

  const signup = async (username) => {
    try {
      const response = await axios.post(
        `${import.meta.env.VITE_API_DOMAIN}/users`,
        { username },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      if (response.status === 201) {
        const userData = {
          userId: response.data._id,
          username: response.data.username,
        };
        setAuth(userData);
        navigate("/");
      } else {
        throw new Error("Failed to create User.");
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  const logout = async () => {
    try {
      const response = await axios.delete(
        `${import.meta.env.VITE_API_DOMAIN}/users/${auth.userId}`,
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      if (response.status === 200) {
        setAuth(null);
        localStorage.removeItem("auth");
        navigate("/");
      } else {
        throw new Error("Failed to delete User.");
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  return (
    <AuthContext.Provider value={{ auth, signup, logout }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  return useContext(AuthContext);
};
