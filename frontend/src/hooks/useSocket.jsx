import { createContext, useContext, useEffect, useState } from "react";
import Ably from "ably";

const SocketContext = createContext();

export const SocketProvider = ({ children }) => {
  const [socket, setSocket] = useState(null);
  const [isConnected, setIsConnected] = useState(false);

  useEffect(() => {
    const ablyInstance = new Ably.Realtime({
      key: import.meta.env.VITE_ABLY_API_KEY,
    });

    ablyInstance.connection.on("connected", () => {
      setIsConnected(true);
    });

    ablyInstance.connection.on("disconnected", () => {
      setIsConnected(false);
    });

    setSocket(ablyInstance);

    return () => {
      if (ablyInstance) {
        ablyInstance.close();
      }
    };
  }, []);

  return (
    <SocketContext.Provider value={{ socket, isConnected }}>
      {children}
    </SocketContext.Provider>
  );
};

export const useSocket = () => {
  return useContext(SocketContext);
};