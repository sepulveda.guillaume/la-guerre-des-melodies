import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import { BrowserRouter as Router } from "react-router-dom";
import { AuthProvider } from "./hooks/useAuth";
import { SocketProvider } from "./hooks/useSocket.jsx";
import { ChakraProvider } from "@chakra-ui/react";
import theme from "./utils/theme.js";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <ChakraProvider theme={theme}>
      <SocketProvider>
        <Router>
          <AuthProvider>
            <App />
          </AuthProvider>
        </Router>
      </SocketProvider>
    </ChakraProvider>
  </React.StrictMode>
);
