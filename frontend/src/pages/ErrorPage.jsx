import { motion } from "framer-motion";
import { Center, Heading, Text } from "@chakra-ui/react";

export default function ErrorPage() {
  return (
    <motion.div
      className="container py-4 px-3 mx-auto"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.5 }}
    >
      <Center h="100vh" flexDirection="column">
        <Heading as="h2" size="xl" color="red.500" mb={4}>404</Heading>
        <Text fontSize="lg" color="white">Désolé la page que vous avez demandée n&apos;existe pas</Text>
      </Center>
    </motion.div>
  );
}
