import { useState, useEffect, useCallback } from "react";
import { useLocation } from "react-router-dom";
import GameCountdown from "../components/GameCountdown";
import QuestionForm from "../components/QuestionForm";
import ScoreBoard from "../components/ScoreBoard";
import { useAuth } from "../hooks/useAuth";
import { motion } from "framer-motion";
import {
  Box,
  Button,
  Container,
  Flex,
  Text,
  useColorMode,
  useTheme,
} from "@chakra-ui/react";
import DarkModeToggle from "../components/DarkModeToggle";
import { checkAnswer, fetchSongUrl } from "../services/api";
import HowlerAudioPlayer from "../components/HowlerAudioPlayer";

export default function GamePage() {
  const location = useLocation();
  const { auth } = useAuth();
  const timeAfterEachSong = 5;

  const theme = useTheme();
  const colorMode = useColorMode().colorMode;

  const [songs, setSongs] = useState([]);
  const [currentSong, setCurrentSong] = useState(null);
  const [currentSongIndex, setCurrentSongIndex] = useState(0);
  const [formData, setFormData] = useState({
    choiceTitle: "",
    choiceArtist: "",
  });
  const [isCorrectTitle, setIsCorrectTitle] = useState(false);
  const [isCorrectArtist, setIsCorrectArtist] = useState(false);
  const [correctTitle, setCorrectTitle] = useState(null);
  const [correctArtist, setCorrectArtist] = useState(null);
  const [usersScore, setUsersScore] = useState(null);
  const [submitted, setSubmitted] = useState(false);
  const [isFinished, setIsFinished] = useState(false);
  const [countdown, setCountdown] = useState(
    location.state.gameParams.duration - timeAfterEachSong
  );
  const [newQuestionCountdown, setNewQuestionCountdown] =
    useState(timeAfterEachSong);
  const [showScores, setShowScores] = useState(false);

  const handleSubmit = useCallback(async () => {
    const response = await checkAnswer({
      title: formData.choiceTitle,
      artist: formData.choiceArtist,
      gameId: location.state.game._id,
      username: auth.username,
    });
    setSubmitted(true);
    setIsCorrectTitle(response.userCorrectTitle);
    setIsCorrectArtist(response.userCorrectArtist);
    setCorrectTitle(response.correctTitle);
    setCorrectArtist(response.correctArtist);
    setUsersScore(response.game.scores);
    if (response.game.status === "finished") {
      setIsFinished(true);
    }
  }, [
    auth.username,
    formData.choiceArtist,
    formData.choiceTitle,
    location.state.game._id,
  ]);

  const handleNextSong = useCallback(() => {
    setCurrentSongIndex(currentSongIndex + 1);
    setFormData({ choiceTitle: "", choiceArtist: "" });
    setCorrectTitle(null);
    setCorrectArtist(null);
    setIsCorrectTitle(false);
    setIsCorrectArtist(false);
    setUsersScore(null);
    setSubmitted(false);
    setCountdown(location.state.gameParams.duration - timeAfterEachSong);
    setNewQuestionCountdown(timeAfterEachSong);
  }, [
    currentSongIndex,
    setCurrentSongIndex,
    location.state.gameParams.duration,
  ]);

  useEffect(() => {
    const fetchSongData = async () => {
      try {
        const songIds = location.state.game.songsIds;
        setSongs(songIds);
      } catch (error) {
        console.error("Error fetching song data:", error);
      }
    };
    fetchSongData();
  }, [location]);

  useEffect(() => {
    const fetchCurrentSongUrl = async () => {
      if (currentSongIndex < songs.length) {
        const songId = songs[currentSongIndex];
        const songUrl = await fetchSongUrl(songId);
        setCurrentSong(songUrl);
      }
    };
    fetchCurrentSongUrl();
  }, [currentSongIndex, songs]);

  useEffect(() => {
    let timer;

    if (countdown > 0 && !isFinished) {
      timer = setInterval(() => {
        setCountdown((prevCountdown) => prevCountdown - 1);
      }, 1000);
    }
    if (countdown === 0 && !isFinished) {
      handleSubmit();
      clearInterval(timer);
    }
    return () => {
      clearInterval(timer);
    };
  }, [countdown, isFinished, handleSubmit]);

  useEffect(() => {
    let newQuestionTimer;

    if (newQuestionCountdown > 0 && countdown === 0 && !isFinished) {
      newQuestionTimer = setInterval(() => {
        setNewQuestionCountdown((prevCountdown) => prevCountdown - 1);
      }, 1000);
    }
    if (newQuestionCountdown === 0 && countdown === 0 && !isFinished) {
      handleNextSong();
      clearInterval(newQuestionTimer);
    }
    return () => {
      clearInterval(newQuestionTimer);
    };
  }, [newQuestionCountdown, countdown, isFinished, handleNextSong]);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const displayScores = () => {
    setShowScores(true);
  };

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.5 }}
      key={[currentSongIndex, showScores]}
    >
      <Container width={{ base: "100%", md: "50%" }} padding={8} centerContent>
        <DarkModeToggle />
        <Box width="100%">
          {countdown >= 0 && !showScores && (
            <GameCountdown countdown={countdown} size={3} />
          )}
          {!showScores && (
            <>
              <HowlerAudioPlayer audioSrc={currentSong} />
              <QuestionForm
                isCorrectArtist={isCorrectArtist}
                isCorrectTitle={isCorrectTitle}
                correctArtist={correctArtist}
                correctTitle={correctTitle}
                submitted={submitted}
                formData={formData}
                handleInputChange={handleInputChange}
                handleSubmit={handleSubmit}
              />
              {!isFinished && countdown === 0 && submitted && (
                <Flex
                  marginTop={4}
                  flexDirection={"column"}
                  justifyContent={"center"}
                  alignItems={"center"}
                >
                  Prochaine question dans{" "}
                  {newQuestionCountdown >= 0 && (
                    <Box marginTop={2}>
                      <GameCountdown
                        countdown={newQuestionCountdown}
                        size={2}
                      />
                    </Box>
                  )}
                  <Text
                    marginTop={2}
                    fontWeight={"bold"}
                    color={theme.colors[colorMode].txtGray}
                  >
                    {songs.length - currentSongIndex - 1 === 1
                      ? "1 chanson restante"
                      : `${
                          songs.length - currentSongIndex - 1
                        } chansons restantes`}
                  </Text>
                </Flex>
              )}
              {isFinished && (
                <Flex
                  marginTop={2}
                  flexDirection={"column"}
                  justifyContent={"center"}
                  alignItems={"center"}
                >
                  <Text
                    marginTop={4}
                    fontWeight={"bold"}
                    color={theme.colors[colorMode].txtGray}
                  >
                    Fin de la partie
                  </Text>
                  <Button
                    onClick={displayScores}
                    bgColor={theme.colors[colorMode].bgRed}
                    _hover={{
                      bgColor: theme.colors[colorMode].bgRedHover,
                    }}
                    marginTop={4}
                  >
                    Voir les scores
                  </Button>
                </Flex>
              )}
            </>
          )}
          {usersScore && !isFinished && (
            <Flex marginTop={2} justifyContent={"center"} alignItems={"center"}>
              {Object.entries(usersScore)
                .filter(([username]) => username === auth.username)
                .map(([username, score]) => (
                  <Text
                    marginTop={4}
                    fontWeight={"bold"}
                    key={username}
                    color={theme.colors[colorMode].txtRed}
                  >
                    Votre score {username}: {score}
                  </Text>
                ))}
            </Flex>
          )}
          {showScores && <ScoreBoard usersScore={usersScore} />}
        </Box>
      </Container>
    </motion.div>
  );
}
