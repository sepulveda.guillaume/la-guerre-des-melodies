import { Box, Container } from "@chakra-ui/react";
import { Outlet, Navigate } from "react-router-dom";
import Navbar from "../components/Navbar";
import { useAuth } from "../hooks/useAuth";
import { motion } from "framer-motion";
import DarkModeToggle from "../components/DarkModeToggle";
import Header from "../components/Header";

export default function Layout() {
  const { auth } = useAuth();

  return (
    <Container
      as={motion.div}
      py={4}
      px={3}
      maxW="container.lg"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.5 }}
    >
      <Box as="header" py={5} textAlign="center">
        <Header />
        <DarkModeToggle />
      </Box>
      <Container py={{base: 0, md: 4}} px={3} maxW="container.lg">
        {auth ? (
          <>
            <Navbar auth={auth} />
            <Box my={4}>
              <Outlet />
            </Box>
          </>
        ) : (
          <Navigate to="/login" />
        )}
      </Container>
    </Container>
  );
}
