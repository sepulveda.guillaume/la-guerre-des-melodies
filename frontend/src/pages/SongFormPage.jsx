import SongFormSpotify from "../components/SongFormSpotify";
import SongFormYoutube from "../components/SongFormYoutube";
import { motion } from "framer-motion";
import {
  Heading,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  useColorMode,
  useTheme,
} from "@chakra-ui/react";

export default function SongFormPage() {
  const theme = useTheme();
  const colorMode = useColorMode().colorMode;

  return (
    <motion.div
      className="container"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.5 }}
    >
      <Heading as="h2" size="lg" mb={6}>
        Ajouter une chanson
      </Heading>
      <Tabs isFitted variant="enclosed">
        <TabList justifyContent="center" mb={6}>
          <Tab
            _selected={{
              bg: theme.colors[colorMode].bgGreen,
            }}
          >
            Spotify
          </Tab>
          <Tab
            _selected={{
              bg: theme.colors[colorMode].bgRed,
            }}
          >
            Youtube
          </Tab>
        </TabList>
        <TabPanels display="flex" justifyContent="center" marginTop={8}>
          <TabPanel width={{ base: "100%", md: "50%" }} padding={0}>
            <SongFormSpotify />
          </TabPanel>
          <TabPanel width={{ base: "100%", md: "50%" }} padding={0}>
            <SongFormYoutube />
          </TabPanel>
        </TabPanels>
      </Tabs>
    </motion.div>
  );
}
