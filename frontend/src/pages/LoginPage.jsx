import { useState } from "react";
import { useAuth } from "../hooks/useAuth";
import { Navigate } from "react-router-dom";
import {
  Button,
  FormControl,
  FormErrorMessage,
  Input,
  Flex,
  useTheme,
  useColorMode,
} from "@chakra-ui/react";
import DarkModeToggle from "../components/DarkModeToggle";
import ModalGame from "../components/ModalGame";

export default function LoginPage() {
  const theme = useTheme();
  const colorMode = useColorMode().colorMode;

  const { auth, signup } = useAuth();

  const [username, setUsername] = useState("");
  const [error, setError] = useState(null);
  const [showModal, setShowModal] = useState(true);

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (!username.trim()) {
      setError("Le nom ne peut pas être vide");
      return;
    }
    await signup(username);
    setUsername("");
    setShowModal(false);
  };

  const footerModal = (
    <Button
      onClick={handleSubmit}
      bgColor={theme.colors[colorMode].bgRed}
      _hover={{ bgColor: theme.colors[colorMode].bgRedHover }}
      width="100%"
    >
      Valider
    </Button>
  );

  return auth ? (
    <Navigate to="/" />
  ) : (
    <Flex justifyContent="center" alignItems="center" height="100vh">
      <DarkModeToggle />
      <ModalGame
        isOpen={showModal}
        onClose={() => setShowModal(false)}
        title="Entrez votre nom"
        footer={footerModal}
      >
        <form onSubmit={handleSubmit}>
          <FormControl isInvalid={error}>
            <Input
              type="text"
              placeholder="Entrez votre nom"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              autoFocus
              borderColor={theme.colors[colorMode].txtGray}
              focusBorderColor={theme.colors[colorMode].bgRed}
              _hover={{
                borderColor: theme.colors[colorMode].bgRed,
              }}
              _placeholder={{
                color: theme.colors[colorMode].txtGray,
              }}
            />
            <FormErrorMessage>{error}</FormErrorMessage>
          </FormControl>
        </form>
      </ModalGame>
    </Flex>
  );
}
