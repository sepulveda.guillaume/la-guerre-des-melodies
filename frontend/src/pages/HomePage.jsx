import { useAuth } from "../hooks/useAuth";
import { useSocket } from "../hooks/useSocket";
import { useEffect, useState } from "react";
import ParticipantsList from "../components/ParticipantsList";
import { useNavigate } from "react-router-dom";
import { motion } from "framer-motion";
import { Heading, Text, useTheme, useColorMode } from "@chakra-ui/react";
import ModalGame from "../components/ModalGame";
import { fetchSongs } from "../services/api";

export default function HomePage() {
  const { auth } = useAuth();
  const { socket, isConnected } = useSocket();
  const navigate = useNavigate();
  const theme = useTheme();
  const colorMode = useColorMode().colorMode;

  const [participants, setParticipants] = useState([]);
  const [gameStarted, setGameStarted] = useState(false);
  const [error, setError] = useState(null);
  const [countdown, setCountdown] = useState(5);
  const [selectedTheme, setSelectedTheme] = useState("");
  const [themes, setThemes] = useState([]);

  const currentUser = participants.find((p) => p.id === auth.userId);

  useEffect(() => {
    if (socket && auth && isConnected) {
      const gameChannel = socket.channels.get("game");

      gameChannel.publish("joinLobby", auth);

      gameChannel.subscribe("updateParticipants", ({ data }) => {
        setParticipants(data);
      });

      const gameStartedHandler = ({ data }) => {
        navigate(`/game/${data.game._id}`, {
          state: {
            game: data.game,
            gameParams: data.gameParams,
          },
        });
      };
      gameChannel.subscribe("gameStarted", gameStartedHandler);

      return () => {
        gameChannel.publish("leaveLobby", auth);
        gameChannel.unsubscribe("updateParticipants");
        gameChannel.unsubscribe("gameStarted", gameStartedHandler);
      };
    }
  }, [socket, auth, navigate, isConnected]);

  useEffect(() => {
    const fetchSongsData = async () => {
      try {
        const songs = await fetchSongs();

        if (songs.length === 0) {
          setError("Il n'y a pas de musique disponible pour jouer.");
        }

        setThemes(new Set(songs.map((song) => song.themes).flat()));
      } catch (error) {
        console.error("Erreur lors du démarrage du jeu :", error);
      }
    };
    fetchSongsData();
  }, []);

  const toggleReady = () => {
    socket.channels.get("game").publish("toggleReady", auth);
  };

  useEffect(() => {
    let timer;

    const allReady = () => {
      return participants.length > 0 && participants.every((p) => p.ready);
    };

    if (error) {
      return;
    }

    if (allReady()) {
      setGameStarted(true);
      setError(null);
    }

    if (gameStarted) {
      timer = setInterval(() => {
        setCountdown((prevCountdown) => {
          if (prevCountdown <= 1) {
            clearInterval(timer);
            socket.channels.get("game").publish("startGame", selectedTheme);
            return prevCountdown;
          } else {
            return prevCountdown - 1;
          }
        });
      }, 1000);
    }

    return () => {
      clearInterval(timer);
    };
  }, [participants, error, gameStarted, socket]);

  const handleThemeChange = (e) => {
    setSelectedTheme(e.target.value);
  };

  return (
    <motion.div
      className="container"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.5 }}
    >
      <Heading as="h2" size="lg" mb={4}>
        Blind test
      </Heading>
      <Text mb={4} textAlign={"justify"}>
        Testez vos connaissances musicales{" "}
        <Text
          as="span"
          color={theme.colors[colorMode].txtRed}
          fontWeight={"bold"}
        >
          {auth.username}
        </Text>{" "}
        en écoutant des extraits de chansons et en essayant de deviner leur
        titre et leur interprète.
      </Text>
      <Text
        mb={6}
        textAlign={"justify"}
        color={theme.colors[colorMode].txtGray}
      >
        <Text
          as={"u"}
          fontWeight={"bold"}
          color={theme.colors[colorMode].txtReverse}
          fontSize={"lg"}
        >
          Règles:
        </Text>
        <br />- Vous avez{" "}
        <Text
          as={"span"}
          fontWeight={"bold"}
          color={theme.colors[colorMode].txtReverse}
        >
          30 secondes
        </Text>{" "}
        pour deviner le titre et l&apos;artiste de la chanson.
        <br />- Si vous trouvez{" "}
        <Text
          as={"span"}
          fontWeight={"bold"}
          color={theme.colors[colorMode].txtReverse}
        >
          le titre
        </Text>{" "}
        ou{" "}
        <Text
          as={"span"}
          fontWeight={"bold"}
          color={theme.colors[colorMode].txtReverse}
        >
          l&apos;interprète
        </Text>{" "}
        vous gagnez{" "}
        <Text
          as={"span"}
          fontWeight={"bold"}
          color={theme.colors[colorMode].txtReverse}
        >
          1 point
        </Text>
        . Si vous trouvez les{" "}
        <Text
          as={"span"}
          fontWeight={"bold"}
          color={theme.colors[colorMode].txtReverse}
        >
          deux
        </Text>
        , vous remportez{" "}
        <Text
          as={"span"}
          fontWeight={"bold"}
          color={theme.colors[colorMode].txtReverse}
        >
          3 points
        </Text>
        .
        <br />- Le joueur avec le plus de points à la fin du jeu{" "}
        <Text
          as={"span"}
          fontWeight={"bold"}
          color={theme.colors[colorMode].txtReverse}
        >
          remporte
        </Text>{" "}
        la partie.
      </Text>
      {error && (
        <Text color={theme.colors[colorMode].txtRed} mt={3}>
          {error}
        </Text>
      )}
      <Text mb={2}>Thème:</Text>
      <select value={selectedTheme} onChange={handleThemeChange}>
        <option value="">Aléatoire</option>
        {[...themes].map((theme) => (
          <option key={theme} value={theme}>
            {theme}
          </option>
        ))}
      </select>
      <ParticipantsList
        participants={participants}
        toggleReady={toggleReady}
        currentUser={currentUser}
      />
      <ModalGame
        isOpen={gameStarted}
        title="Démarrage du jeu dans"
        countdown={countdown}
      />
    </motion.div>
  );
}
