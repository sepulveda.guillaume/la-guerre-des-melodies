import { useState, useEffect } from "react";
import SongsList from "../components/SongsList";
import { motion } from "framer-motion";
import { Heading, Text, Box, useTheme, useColorMode } from "@chakra-ui/react";
import Spinner from "../components/Spinner";
import { deleteSong, fetchSongs } from "../services/api";

export default function SongsPage() {
  const [songs, setSongs] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const theme = useTheme();
  const colorMode = useColorMode().colorMode;

  useEffect(() => {
    setLoading(true);
    const fetchSongsData = async () => {
      try {
        const songs = await fetchSongs();
        setSongs(songs);
      } catch (error) {
        setError(error.message);
      } finally {
        setLoading(false);
      }
    };
    fetchSongsData();
  }, []);

  const handleDelete = async (songId) => {
    try {
      await deleteSong(songId);
      const updatedSongs = songs.filter((song) => song._id !== songId);
      setSongs(updatedSongs);
    } catch (error) {
      setError(error.message);
    }
  };

  return (
    <motion.div
      className="container"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.5 }}
    >
      <Heading as="h2" size="lg" mb={4}>
        Chansons
      </Heading>
      <Text mb={4}>
        Retrouvez ici la liste des chansons disponibles dans notre blind test.
      </Text>
      {error && (
        <Box>
          <Text color={theme.colors[colorMode].txtRed}>{error}</Text>
        </Box>
      )}
      {loading ? (
        <Spinner />
      ) : (
        <>
          {songs.length === 0 ? (
            <Box>
              <Text color={theme.colors[colorMode].txtRed}>
                Aucune chanson trouvée.
              </Text>
            </Box>
          ) : (
            <SongsList songs={songs} onDelete={handleDelete} totalItems={songs.length} />
          )}
        </>
      )}
    </motion.div>
  );
}
