import axios from "axios";

const API_DOMAIN = import.meta.env.VITE_API_DOMAIN;
const SPOTIFY_CLIENT_ID = import.meta.env.VITE_SPOTIFY_CLIENT_ID;
const SPOTIFY_CLIENT_SECRET = import.meta.env.VITE_SPOTIFY_CLIENT_SECRET;
const YOUTUBE_API_KEY = import.meta.env.VITE_YOUTUBE_API_KEY;

// Game related API functions
const checkAnswer = async (answerData) => {
  const response = await axios.post(
    `${API_DOMAIN}/games/checkAnswer`,
    answerData
  );
  return response.data;
};

const fetchSongUrl = async (songId) => {
  const response = await axios.get(`${API_DOMAIN}/songs/${songId}`);
  return response.data.audioFilePath;
};

const fetchSongs = async () => {
  const response = await axios.get(`${API_DOMAIN}/songs`);
  return response.data;
};

const deleteSong = async (songId) => {
  await axios.delete(`${API_DOMAIN}/songs/${songId}`);
};

// Spotify related API functions
const fetchSpotifyToken = async () => {
  const response = await axios.post(
    "https://accounts.spotify.com/api/token",
    `grant_type=client_credentials&client_id=${SPOTIFY_CLIENT_ID}&client_secret=${SPOTIFY_CLIENT_SECRET}`,
    {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    }
  );
  return response.data.access_token;
};

const searchSpotifySongs = async (accessToken, searchTerm) => {
  const response = await axios.get("https://api.spotify.com/v1/search", {
    params: {
      q: searchTerm,
      type: "track",
    },
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });

  const filteredSongs = response.data.tracks.items.filter(
    (song) => song.preview_url !== null
  );

  return filteredSongs.map((song) => ({
    value: song.id,
    label: `${song.name} - ${song.artists[0].name}`,
    imageUrl: song.album.images[2].url,
    url: song.preview_url,
  }));
};

// YouTube related API functions
const searchYoutubeVideos = async (searchTerm) => {
  const response = await axios.get(
    `https://www.googleapis.com/youtube/v3/search?q=${searchTerm}&part=snippet&maxResults=10&key=${YOUTUBE_API_KEY}`
  );

  return response.data.items.map((video) => ({
    videoId: video.id.videoId,
    label: video.snippet.title,
    thumbnailUrl: video.snippet.thumbnails.default.url,
  }));
};

// For uploading songs
const uploadSong = async (title, artist, url, type, themes) => {
  if(themes?.length === 0 || !themes) {
    throw new Error("Veuillez sélectionner au moins un thème.");
  }
  const response = await axios.post(`${API_DOMAIN}/songs`, {
    title,
    artist,
    url,
    type,
    themes,
  });

  return response.status === 201;
};

// Export all functions
export {
  checkAnswer,
  fetchSongUrl,
  fetchSongs,
  deleteSong,
  fetchSpotifyToken,
  searchSpotifySongs,
  searchYoutubeVideos,
  uploadSong,
};
