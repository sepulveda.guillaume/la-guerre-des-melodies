import {
  Button,
  Flex,
  useColorMode,
  IconButton,
  useTheme,
  Image,
} from "@chakra-ui/react";
import { HamburgerIcon, CloseIcon } from "@chakra-ui/icons";
import { useState } from "react";
import { Link as RouterLink } from "react-router-dom";
import { useAuth } from "../hooks/useAuth";
import { motion } from "framer-motion";
import { useSocket } from "../hooks/useSocket";

export default function NavBar() {
  const [showMenu, setShowMenu] = useState(false);
  const { auth, logout } = useAuth();
  const { socket, isConnected } = useSocket();
  const theme = useTheme();
  const colorMode = useColorMode().colorMode;

  const sectionInfo = [
    { label: "Accueil", href: "/" },
    { label: "Chansons", href: "/songs" },
    { label: "Ajouter une chanson", href: "/new" },
  ];

  const activeIndex = sectionInfo.findIndex(
    (section) => section.href === location.pathname
  );

  const handleLogout = async () => {
    await logout(auth.userId);
    if (socket && isConnected) {
      const gameChannel = socket.channels.get("game");
      gameChannel.publish("leaveLobby", auth);
    }
  };

  return (
    <Flex
      alignItems={{ md: "center" }}
      justifyContent={{ base: "flex-start", md: "space-between" }}
      zIndex={1000}
      minWidth={"100%"}
      padding={2}
    >
      <IconButton
        display={{ base: "block", md: "none" }}
        aria-label="Open menu"
        icon={showMenu ? <CloseIcon /> : <HamburgerIcon />}
        onClick={() => setShowMenu(!showMenu)}
        marginRight={2}
      />
      <RouterLink to="/">
        <Image
          src="/song.svg"
          alt="logo"
          width={{ base: 10, md: 45 }}
          height={{ base: 10, md: 45 }}
          marginRight={4}
        />
      </RouterLink>
      <Flex
        display={{ base: showMenu ? "flex" : "none", md: "flex" }}
        wrap={"wrap"}
        direction={"row"}
        justifyContent={"space-between"}
        alignItems={"center"}
        marginRight={{ base: 0, md: showMenu ? 2 : 0 }}
        width={"100%"}
      >
        <Flex
          flexDirection={{ base: "column", md: "row" }}
          justifyContent={{ base: "flex-start", md: "space-between" }}
          width={{ base: "100%", md: "auto" }}
        >
          {sectionInfo.map((section, index) => (
            <motion.button
              key={section.label}
              whileHover={{ scale: 1.05 }}
              whileTap={{ scale: 0.95 }}
              style={{ width: "fit-content" }}
            >
              <Button
                as={RouterLink}
                to={section.href}
                variant={"ghost"}
                size={{ base: "md", md: "lg" }}
                colorScheme={activeIndex === index ? "red" : "gray"}
                marginBottom={{ base: 2, md: 0 }}
                style={{ width: "fit-content" }}
              >
                {section.label}
              </Button>
              {activeIndex === index && (
                <motion.div
                  layoutId="underline"
                  initial={{ opacity: 0 }}
                  animate={{ opacity: 1 }}
                  exit={{ opacity: 0 }}
                  style={{
                    height: "2px",
                    backgroundColor: theme.colors[colorMode].bgRed,
                    borderRadius: "5px",
                    marginTop: "2px",
                  }}
                />
              )}
            </motion.button>
          ))}
        </Flex>
        <Flex
          justifyContent={{ base: "flex-start", md: "center" }}
          width={{ base: "100%", md: "auto" }}
        >
          <motion.button
            whileHover={{ scale: 1.05 }}
            whileTap={{ scale: 0.95 }}
          >
            <Button
              as={RouterLink}
              variant={"outline"}
              size={{ base: "md", md: "lg" }}
              onClick={handleLogout}
              marginTop={{ base: 2, md: 0 }}
              marginLeft={{ base: 0, md: 2 }}
              borderColor={theme.colors[colorMode].bgRed}
            >
              Déconnexion
            </Button>
          </motion.button>
        </Flex>
      </Flex>
    </Flex>
  );
}
