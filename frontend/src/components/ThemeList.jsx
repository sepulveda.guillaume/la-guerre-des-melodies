import CreatableSelect from "react-select/creatable";

export default function ThemeList({ onSelectedThemesChange }) {
  const themes = [
    { value: "Années 80", label: "Années 80" },
    { value: "Années 90", label: "Années 90" },
    { value: "Années 2000", label: "Années 2000" },
    { value: "Années 2010", label: "Années 2010" },
    { value: "Années 2020", label: "Années 2020" },
    { value: "Rock", label: "Rock" },
    { value: "Pop", label: "Pop" },
    { value: "Jazz", label: "Jazz" },
    { value: "Classique", label: "Classique" },
    { value: "Rap", label: "Rap" },
    { value: "Reggae", label: "Reggae" },
    { value: "Electro", label: "Electro" },
    { value: "Metal", label: "Metal" },
    { value: "Blues", label: "Blues" },
    { value: "Variété", label: "Variété" },
    { value: "R&B", label: "R&B" },
    { value: "Reggaeton", label: "Reggaeton" },
  ];

  const handleSelectChange = (selectedOptions) => {
    const values = selectedOptions.map((option) => option.value);
    onSelectedThemesChange(values);
  };

  const colourStyles = {
    option: (styles, { isFocused }) => {
      return {
        ...styles,
        backgroundColor: isFocused ? "hsl(0, 0%, 90%)" : null,
        color: "#333333",
      };
    },
  };

  return (
    <CreatableSelect
      options={themes}
      onChange={handleSelectChange}
      isMulti
      placeholder="Sélectionnez ou ajoutez un thème"
      isClearable
      isSearchable
      styles={colourStyles}
      closeMenuOnSelect={false}
    />
  );
}
