import ReactH5AudioPlayer from "react-h5-audio-player";
import "react-h5-audio-player/lib/styles.css";
import { useRef, useEffect } from "react";

export default function CustomAudioPlayer({
  audioFilePath,
  onClick,
  isCurrentSong,
  hideControlsStyle,
}) {
  const audioRef = useRef(null);

  useEffect(() => {
    const audioElement = audioRef.current?.audio.current;

    const handleCanPlayThrough = () => {
      if (isCurrentSong) {
        audioElement.play().catch((error) => {
          console.error("Error playing the audio:", error);
        });
      }
    };

    if (audioElement) {
      audioElement.addEventListener('canplaythrough', handleCanPlayThrough);
    }

    return () => {
      if (audioElement) {
        audioElement.removeEventListener('canplaythrough', handleCanPlayThrough);
      }
    };
  }, [isCurrentSong]);

  useEffect(() => {
    const audioElement = audioRef.current?.audio.current;
    if (audioElement) {
      if (isCurrentSong) {
        audioElement.play().catch((error) => {
          console.error("Error playing the audio:", error);
        });
      } else {
        audioElement.pause();
      }
    }
  }, [isCurrentSong]);

  return (
    <ReactH5AudioPlayer
      src={audioFilePath}
      onPlay={onClick}
      ref={audioRef}
      autoPlayAfterSrcChange={true}
      customAdditionalControls={[]}
      style={{
        display: hideControlsStyle ? "none" : "block",
        backgroundColor: "transparent",
        boxShadow: "none",
      }}
    />
  );
}
