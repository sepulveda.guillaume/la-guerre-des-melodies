import { useEffect } from "react";
import { Howl } from "howler";

export default function HowlerAudioPlayer({ audioSrc }) {
  useEffect(() => {
    const sound = new Howl({
      src: [audioSrc],
      autoplay: true,
      html5: true,
    });

    return () => {
      sound.unload();
    };
  }, [audioSrc]);

  return <></>;
}
