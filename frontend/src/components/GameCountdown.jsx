import { motion } from "framer-motion";
import { Text, useColorMode, useTheme } from "@chakra-ui/react";

export default function GameCountdown({ countdown, size }) {
  const theme = useTheme();
  const colorMode = useColorMode().colorMode;

  return (
    <motion.div
      initial={{ scale: 0 }}
      animate={{ scale: 1 }}
      transition={{ duration: 0.5 }}

    >
      <Text
        fontSize={`${size}rem`}
        color={countdown <= 5 ? theme.colors[colorMode].txtRed : theme.colors[colorMode].txtGray}
        textAlign="center"
      >
        {countdown}
      </Text>
    </motion.div>
  );
}
