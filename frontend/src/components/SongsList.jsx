import Song from "./Song";
import { useEffect, useState } from "react";
import { SimpleGrid } from "@chakra-ui/react";
import { motion } from "framer-motion";
import Pagination from "./Pagination";

export default function SongsList({ songs, onDelete, totalItems }) {
  const [currentSong, setCurrentSong] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const songsPerPage = 6;

  const onSongClick = (song) => {
    setCurrentSong(song);
  };

  const startIndex = (currentPage - 1) * songsPerPage;
  const endIndex = startIndex + songsPerPage;
  const currentSongs = songs.slice(startIndex, endIndex);

  const totalPages = Math.ceil(totalItems / songsPerPage);

  useEffect(() => {
    if (currentSongs.length === 0 && currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  }, [currentSongs, currentPage]);

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  return (
    <motion.div
      className="container py-4 px-3 mx-auto"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.5 }}
    >
      <SimpleGrid columns={{ base: 1, sm: 3 }} spacing={4} marginTop={4}>
        {currentSongs.map((song) => (
          <Song
            key={song._id}
            song={song}
            onClick={() => onSongClick(song)}
            isCurrentSong={currentSong === song}
            onDelete={onDelete}
          />
        ))}
      </SimpleGrid>
      <Pagination
        currentPage={currentPage}
        totalPages={totalPages}
        onPageChange={handlePageChange}
      />
    </motion.div>
  );
}
