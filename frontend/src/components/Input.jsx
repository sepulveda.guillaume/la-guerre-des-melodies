import { Box, FormLabel, Input as ChakraInput } from "@chakra-ui/react";

export default function Input({ label, type, id, name, value, onChange }) {
  return (
    <Box mb={4}>
      <FormLabel htmlFor={id}>{label}</FormLabel>
      <ChakraInput
        type={type}
        id={id}
        name={name}
        value={value}
        onChange={onChange}
      />
    </Box>
  );
}
