import { Heading, useColorMode, useTheme } from "@chakra-ui/react";
import { motion } from "framer-motion";

export default function Header() {
  const theme = useTheme();
  const colorMode = useColorMode().colorMode;

  const title = "La Guerre des Mélodies";

  const letters = title.split("");
  const stagger = {
    visible: {
      transition: {
        staggerChildren: 0.08,
      },
    },
  };

  const variants = {
    hidden: {
      opacity: 0,
      y: 50,
    },
    visible: {
      opacity: 1,
      y: 0,
    },
  };

  return (
    <Heading
      as={motion.h1}
      initial="hidden"
      animate="visible"
      variants={stagger}
      display={"flex"}
      flexWrap={"wrap"}
      justifyContent={"center"}
      alignItems={"center"}
      paddingTop={{ base: 8, md: 4}}
      paddingX={{ base: 8, md: 4}}
      fontSize={{ base: "5xl", lg: "6xl" }}
      color={theme.colors[colorMode].txtRed}
    >
      {letters.map((letter, index) => (
        <motion.span key={index} variants={variants}>
          {letter === " " ? "\u00A0" : letter}
        </motion.span>
      ))}
    </Heading>
  );
}
