import {
  Flex,
  Circle,
  Text,
  useColorMode,
  useTheme,
  Button,
} from "@chakra-ui/react";

export default function Participant({ participant, toggleReady, currentUser }) {
  const theme = useTheme();
  const colorMode = useColorMode().colorMode;

  const isCurrentUser = participant?.id === currentUser?.id;

  return (
    <Flex
      flexDirection={"column"}
      alignItems={"center"}
      justifyContent={"center"}
    >
      <Circle
        size="50px"
        bg={theme.colors[colorMode].bgRed}
        mb={2}
        fontWeight="bold"
        fontSize="lg"
        color={"white"}
      >
        {participant.username[0]}
      </Circle>
      <Text marginTop={isCurrentUser ? 0 : 1}>{participant.username}</Text>
      {isCurrentUser ? (
        <Button
          mt={3}
          size="sm"
          onClick={() => toggleReady(participant)}
          variant={"solid"}
          bgColor={
            participant.ready
              ? theme.colors[colorMode].bgGreen
              : theme.colors[colorMode].bgRed
          }
          _hover={
            participant.ready
              ? { bgColor: theme.colors[colorMode].bgGreenHover }
              : { bgColor: theme.colors[colorMode].bgRedHover }
          }
        >
          {participant.ready ? (
            <Text fontWeight={"bold"}>Prêt</Text>
          ) : (
            <Text fontWeight={"bold"}>Pas prêt</Text>
          )}
        </Button>
      ) : (
        <>
          {participant.ready ? (
            <Text
              color={theme.colors[colorMode].txtGreen}
              fontWeight={"bold"}
              fontSize={"sm"}
              mt={3}
            >
              Prêt
            </Text>
          ) : (
            <Text
              color={theme.colors[colorMode].txtRed}
              fontWeight={"bold"}
              fontSize={"sm"}
              mt={3}
            >
              Pas prêt
            </Text>
          )}
        </>
      )}
    </Flex>
  );
}
