import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMusic, faUser } from "@fortawesome/free-solid-svg-icons";
import { motion } from "framer-motion";
import {
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  InputLeftElement,
  useColorMode,
  useTheme,
} from "@chakra-ui/react";

export default function QuestionForm({
  formData,
  isCorrectTitle,
  isCorrectArtist,
  correctTitle,
  correctArtist,
  submitted,
  handleInputChange,
  handleSubmit,
}) {
  const theme = useTheme();
  const colorMode = useColorMode().colorMode;

  return (
    <motion.form
      onSubmit={handleSubmit}
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.5 }}
    >
      <FormControl marginTop={4}>
        <FormLabel htmlFor="choiceTitle">Titre</FormLabel>
        <InputGroup>
          <Input
            id="choiceTitle"
            type="text"
            name="choiceTitle"
            placeholder="Titre"
            value={!submitted ? formData.choiceTitle : correctTitle}
            onChange={handleInputChange}
            disabled={submitted}
            style={{ opacity: 1 }}
            _focus={{
              borderColor: theme.colors[colorMode].txtRed,
              boxShadow: "none",
            }}
            color={
              submitted && isCorrectTitle
                ? theme.colors[colorMode].txtGreen
                : submitted && !isCorrectTitle
                ? theme.colors[colorMode].txtRed
                : theme.colors[colorMode].txtReverse
            }
            _placeholder={{
              color: theme.colors[colorMode].txtGray,
            }}
            borderColor={theme.colors[colorMode].txtGray}
            _hover={{
              borderColor: theme.colors[colorMode].bgRed,
            }}
          />
          <InputLeftElement pointerEvents="none">
            <FontAwesomeIcon icon={faMusic} />
          </InputLeftElement>
        </InputGroup>
      </FormControl>
      <FormControl mt={4}>
        <FormLabel htmlFor="choiceArtist">Interprète</FormLabel>
        <InputGroup>
          <Input
            id="choiceArtist"
            type="text"
            name="choiceArtist"
            placeholder="Interprète"
            value={!submitted ? formData.choiceArtist : correctArtist}
            onChange={handleInputChange}
            disabled={submitted}
            style={{ opacity: 1 }}
            _focus={{
              borderColor: theme.colors[colorMode].txtRed,
              boxShadow: "none",
            }}
            color={
              submitted && isCorrectArtist
                ? theme.colors[colorMode].txtGreen
                : submitted && !isCorrectArtist
                ? theme.colors[colorMode].txtRed
                : theme.colors[colorMode].txtReverse
            }
            _placeholder={{
              color: theme.colors[colorMode].txtGray,
            }}
            borderColor={theme.colors[colorMode].txtGray}
            _hover={{
              borderColor: theme.colors[colorMode].bgRed,
            }}
          />
          <InputLeftElement pointerEvents="none">
            <FontAwesomeIcon icon={faUser} />
          </InputLeftElement>
        </InputGroup>
      </FormControl>
    </motion.form>
  );
}
