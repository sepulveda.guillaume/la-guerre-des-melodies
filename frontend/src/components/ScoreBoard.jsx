import { Link } from "react-router-dom";
import {
  Link as ChakraLink,
  Text,
  Heading,
  Container,
  useTheme,
  useColorMode,
  Box,
} from "@chakra-ui/react";
import { motion } from "framer-motion";

export default function ScoreBoard({ usersScore }) {
  const theme = useTheme();
  const colorMode = useColorMode().colorMode;

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.5 }}
    >
      <Container padding={8} centerContent>
        <Heading as="h2" size="xl" mb={6}>
          Score final
        </Heading>
        <ChakraLink
          as={Link}
          to="/"
          color={theme.colors[colorMode].txtRed}
          fontWeight="bold"
          fontSize="2xl"
          mb={6}
        >
          Rejouer
        </ChakraLink>{" "}
        <Box>
          {Object.entries(usersScore)
            .map(([username, score], index) => ({ username, score, index }))
            .sort((a, b) => b.score - a.score)
            .map(({ username, score, index }) => {
              let textColor;
              if (index === 0) {
                textColor = theme.colors[colorMode].txtGold;
              } else if (index === 1) {
                textColor = theme.colors[colorMode].txtSilver;
              } else if (index === 2) {
                textColor = theme.colors[colorMode].txtBronze;
              } else {
                textColor = theme.colors[colorMode].txtGray;
              }
              return (
                <Text fontWeight="bold" color={textColor} key={index}>
                  {index + 1}. {username}: {score}
                </Text>
              );
            })}
        </Box>
      </Container>
    </motion.div>
  );
}
