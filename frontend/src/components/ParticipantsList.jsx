import Participant from "./Participant";
import { Box, Grid, Text, useColorMode, useTheme } from "@chakra-ui/react";

export default function ParticipantsList({ participants, toggleReady, currentUser }) {
  const theme = useTheme();
  const colorMode = useColorMode().colorMode;

  return (
    <Box
      my={8}
      p={6}
      bgColor={theme.colors[colorMode].txtGrayReverse}
      borderRadius={"0.375rem"}
    >
      <Text
        fontSize={{ base: "xl", md: "2xl" }}
        fontWeight="bold"
        marginBottom={6}
      >
        Participants
      </Text>
      <Grid
        templateColumns="repeat(auto-fill, minmax(100px, 1fr))"
        gap={4}
        justifyItems="center"
        alignItems={"start"}
      >
        {participants.map((participant) => (
          <Participant
            key={participant.id}
            participant={participant}
            toggleReady={toggleReady}
            currentUser={currentUser}
          />
        ))}
      </Grid>
    </Box>
  );
}
