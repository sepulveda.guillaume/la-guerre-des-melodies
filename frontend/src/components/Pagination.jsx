import {
  Box,
  Button,
  ButtonGroup,
  IconButton,
  useColorMode,
  useTheme,
} from "@chakra-ui/react";
import {
  ArrowLeftIcon,
  ArrowRightIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
} from "@chakra-ui/icons";

export default function Pagination({ currentPage, totalPages, onPageChange }) {
  const pagesToShow = 5;
  let startPage = Math.max(1, currentPage - Math.floor(pagesToShow / 2));
  let endPage = Math.min(totalPages, startPage + pagesToShow - 1);

  if (totalPages <= pagesToShow) {
    startPage = 1;
    endPage = totalPages;
  } else {
    if (endPage === totalPages && startPage !== 1) {
      startPage = totalPages - (pagesToShow - 1);
    }
    if (startPage === 1 && endPage !== totalPages) {
      endPage = pagesToShow;
    }
  }

  const pages = [];

  for (let i = startPage; i <= endPage; i++) {
    pages.push(i);
  }

  const theme = useTheme();
  const colorMode = useColorMode().colorMode;

  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="center"
      mt={4}
      transition="all 0.3s ease"
    >
      <ButtonGroup>
        <IconButton
          icon={<ArrowLeftIcon />}
          onClick={() => onPageChange(1)}
          isDisabled={currentPage === 1}
          aria-label="Première page"
        />
        <IconButton
          icon={<ChevronLeftIcon />}
          onClick={() => onPageChange(currentPage - 1)}
          isDisabled={currentPage === 1}
          aria-label="Page précédente"
        />
        {pages.map((page) => (
          <Button
            key={page}
            onClick={() => onPageChange(page)}
            isActive={currentPage === page}
            bgColor={
              currentPage === page
                ? `${theme.colors[colorMode].bgRed} !important`
                : `${theme.colors[colorMode].bgGray} !important`
            }
          >
            {page}
          </Button>
        ))}
        <IconButton
          icon={<ChevronRightIcon />}
          onClick={() => onPageChange(currentPage + 1)}
          isDisabled={currentPage === totalPages}
          aria-label="Page suivante"
        />
        <IconButton
          icon={<ArrowRightIcon />}
          onClick={() => onPageChange(totalPages)}
          isDisabled={currentPage === totalPages}
          aria-label="Dernière page"
        />
      </ButtonGroup>
    </Box>
  );
}
