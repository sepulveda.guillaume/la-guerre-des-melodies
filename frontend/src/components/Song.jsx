import { useState } from "react";
import CustomAudioPlayer from "./CustomAudioPlayer";
import { Box, Button, Text, useTheme, useColorMode } from "@chakra-ui/react";
import { motion } from "framer-motion";

export default function Song({ song, isCurrentSong, onClick, onDelete }) {
  const { _id, title, artist, audioFilePath, themes } = song;
  const themeCss = useTheme();
  const colorMode = useColorMode().colorMode;

  const [deleteClicked, setDeleteClicked] = useState(false);

  const handleDeleteClick = async () => {
    if (!deleteClicked) {
      setDeleteClicked(true);
      await onDelete(_id);
    }
  };

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.3 }}
    >
      <Box
        p={4}
        borderRadius="lg"
        boxShadow={`0px 0px 10px ${themeCss.colors[colorMode].boxShadow}`}
        width="100%"
        position="relative"
      >
        <Text
          fontSize="lg"
          fontWeight="bold"
          mb={2}
          maxWidth={"calc(100% - 120px)"}
        >
          {title}
        </Text>
        <Text fontSize="md" color={themeCss.colors[colorMode].txtGray}>
          {artist}
        </Text>
        <Box mt={4}>
          <CustomAudioPlayer
            audioFilePath={audioFilePath}
            onClick={onClick}
            isCurrentSong={isCurrentSong}
          />
        </Box>
        <Button
          position="absolute"
          top={2}
          right={2}
          m={2}
          bgColor={themeCss.colors[colorMode].bgRed}
          _hover={{ bgColor: themeCss.colors[colorMode].bgRedHover }}
          onClick={handleDeleteClick}
          isDisabled={deleteClicked}
        >
          {deleteClicked ? "Suppression..." : "Supprimer"}
        </Button>
        {!themes ? (
          <Text
            fontSize="md"
            textAlign={"center"}
            color={themeCss.colors[colorMode].txtRed}
          >
            Aucun thème
          </Text>
        ) : (
          <Text fontSize="md" color={themeCss.colors[colorMode].txtRed} textAlign={"center"}>
            {themes.join(", ")}
          </Text>
        )}
      </Box>
    </motion.div>
  );
}
