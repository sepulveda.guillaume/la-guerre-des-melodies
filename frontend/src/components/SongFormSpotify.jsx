import { useEffect, useState } from "react";
import Select from "react-select";
import CustomAudioPlayer from "./CustomAudioPlayer";
import {
  Box,
  Button,
  FormControl,
  Flex,
  useTheme,
  useColorMode,
  Image,
  Text,
} from "@chakra-ui/react";
import Spinner from "./Spinner";
import {
  fetchSpotifyToken,
  searchSpotifySongs,
  uploadSong,
} from "../services/api";
import ThemeList from "./ThemeList";

export default function SongFormSpotify() {
  const [accessToken, setAccessToken] = useState(null);
  const [searchTerm, setSearchTerm] = useState("");
  const [songs, setSongs] = useState([]);
  const [selectedSong, setSelectedSong] = useState(null);
  const [successMessage, setSuccessMessage] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const theme = useTheme();
  const colorMode = useColorMode().colorMode;

  useEffect(() => {
    const fetchToken = async () => {
      try {
        const token = await fetchSpotifyToken();
        setAccessToken(token);
      } catch (error) {
        console.error("Error fetching access token:", error);
        setError(error);
      }
    };
    fetchToken();
  }, []);

  useEffect(() => {
    const searchSongs = async () => {
      try {
        const songs = await searchSpotifySongs(accessToken, searchTerm);
        setSongs(songs);
      } catch (error) {
        console.error("Error searching songs:", error);
        setError(error);
      }
    };

    if (searchTerm.length >= 3) {
      searchSongs();
    } else {
      setSongs([]);
    }
  }, [searchTerm, accessToken]);

  const handleSelectSong = (selectedOption) => {
    setSelectedSong(selectedOption);
  };

  const handleInputChange = (inputValue) => {
    setSuccessMessage(null);
    setError(null);
    setSearchTerm(inputValue);
  };

  const handleUploadSong = async () => {
    setSuccessMessage(null);
    setError(null);

    if (!selectedSong) {
      console.error("Aucune chanson sélectionnée.");
      return;
    }

    setLoading(true);

    try {
      const title = selectedSong.label.split(" - ")[0];
      const artist = selectedSong.label.split(" - ")[1];
      const url = selectedSong.url;
      const type = "mp3";
      const themes = selectedSong.themes;

      const success = await uploadSong(title, artist, url, type, themes);

      if (success) {
        setSuccessMessage("Chanson envoyée avec succès.");
        setSelectedSong({ ...selectedSong, themes: [] });
        setSearchTerm("");
      } else {
        throw new Error("Failed to upload song.");
      }
    } catch (error) {
      console.error("Error uploading song:", error);
      setError(error);
    } finally {
      setLoading(false);
    }
  };

  const handleThemesChange = (selectedThemes) => {
    if (selectedThemes.length === 0) {
      setError({ message: "Veuillez sélectionner au moins un thème." });
    } else {
      setError(null);
    }
    setSelectedSong({ ...selectedSong, themes: selectedThemes });
  };

  const colourStyles = {
    option: (styles, { isFocused }) => {
      return {
        ...styles,
        backgroundColor: isFocused ? "hsl(0, 0%, 90%)" : null,
        color: "#333333",
      };
    },
  };

  const getOptionLabel = (option) => (
    <Flex alignItems="center">
      <Image
        src={option.imageUrl}
        alt="Album"
        style={{ width: "50px", marginRight: "10px" }}
      />
      <Text>{option.label}</Text>
    </Flex>
  );

  return (
    <>
      <FormControl mb={6}>
        <Select
          value={selectedSong}
          onChange={handleSelectSong}
          onInputChange={handleInputChange}
          options={songs}
          styles={colourStyles}
          formatOptionLabel={getOptionLabel}
          isClearable
          isSearchable
          placeholder="Rechercher une chanson..."
        />
      </FormControl>
      {selectedSong && (
        <Box mb={6}>
          <CustomAudioPlayer audioFilePath={selectedSong.url} />
          <Box marginTop={4}>
            <ThemeList onSelectedThemesChange={handleThemesChange} />
          </Box>
          <Box mt={6}>
            <Button
              onClick={handleUploadSong}
              bgColor={theme.colors[colorMode].bgRed}
              _hover={{ bgColor: theme.colors[colorMode].bgRedHover }}
              width="100%"
              mt={2}
            >
              Télécharger la chanson
            </Button>
            {loading && <Spinner />}
            {successMessage && (
              <Box
                color={theme.colors[colorMode].txtGreen}
                mt={4}
                textAlign={"center"}
              >
                {successMessage}
              </Box>
            )}
          </Box>
        </Box>
      )}
      {error && (
        <Box color={theme.colors[colorMode].txtRed} mt={4} textAlign={"center"}>
          {error.message}
        </Box>
      )}
    </>
  );
}
