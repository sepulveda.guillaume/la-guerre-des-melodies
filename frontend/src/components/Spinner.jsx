import { Spinner as SpinnerChakra, useColorMode, Flex } from "@chakra-ui/react";
import { useTheme } from "@emotion/react";

export default function Spinner() {
  const theme = useTheme();
  const colorMode = useColorMode().colorMode;

  return (
    <Flex justify="center" align="center" h="100%" margin={4}>
      <SpinnerChakra
        thickness="4px"
        speed="0.65s"
        emptyColor={theme.colors[colorMode].txtGray}
        color={theme.colors[colorMode].txtRed}
        size="xl"
      />
    </Flex>
  );
}
