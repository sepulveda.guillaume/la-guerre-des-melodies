import { useEffect, useState } from "react";
import Select from "react-select";
import {
  Box,
  Button,
  Flex,
  FormControl,
  Image,
  Text,
  useColorMode,
  useTheme,
} from "@chakra-ui/react";
import Spinner from "./Spinner";
import { searchYoutubeVideos, uploadSong } from "../services/api";
import ThemeList from "./ThemeList";

export default function SongFormYoutube() {
  const [searchTerm, setSearchTerm] = useState("");
  const [videos, setVideos] = useState([]);
  const [selectedVideo, setSelectedVideo] = useState(null);
  const [successMessage, setSuccessMessage] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const theme = useTheme();
  const colorMode = useColorMode().colorMode;

  useEffect(() => {
    const searchVideos = async () => {
      try {
        const videos = await searchYoutubeVideos(searchTerm);
        setVideos(videos);
      } catch (error) {
        console.error("Error searching videos:", error);
        setError(
          error.response?.data?.error?.message.replace(/(<([^>]+)>)/gi, "") ||
            error.message
        );
      }
    };

    if (searchTerm.length >= 3) {
      searchVideos();
    } else {
      setVideos([]);
    }
  }, [searchTerm]);

  const handleSelectVideo = (selectedOption) => {
    setSelectedVideo(selectedOption);
  };

  const handleInputChange = (inputValue) => {
    setSuccessMessage(null);
    setSearchTerm(inputValue);
  };

  const handleUploadVideo = async () => {
    if (!selectedVideo) {
      console.error("Aucune vidéo sélectionnée.");
      return;
    }

    setLoading(true);

    try {
      const parts =
        selectedVideo.label.split(" - ") ||
        selectedVideo.label.split("–") ||
        selectedVideo.label.split("-");

      // remove from title the parenthesis and its content
      parts[0] = parts[0].replace(/\(.*\)/, "");
      const title = parts[1].trim() || selectedVideo.label;
      const artist = parts[0].trim() || "Inconnu";
      const url = `https://www.youtube.com/watch?v=${selectedVideo.videoId}`;
      const type = "mp4";
      const themes = selectedVideo.themes;

      const success = await uploadSong(title, artist, url, type, themes);

      if (success) {
        setSuccessMessage("La vidéo a été téléchargée avec succès.");
      } else {
        throw new Error("Failed to upload song.");
      }
    } catch (error) {
      setError(error.message);
    } finally {
      setLoading(false);
    }
  };

  const colourStyles = {
    option: (styles, { isFocused }) => {
      return {
        ...styles,
        backgroundColor: isFocused ? "hsl(0, 0%, 90%)" : null,
        color: "#333333",
      };
    },
  };

  const getOptionLabel = (option) => (
    <Flex alignItems="center">
      <Image
        src={option.thumbnailUrl}
        alt="Album"
        style={{ width: "50px", marginRight: "10px" }}
      />
      <Text>{option.label}</Text>
    </Flex>
  );

  const handleThemesChange = (selectedThemes) => {
    setSelectedVideo({ ...selectedVideo, themes: selectedThemes });
  };

  return (
    <>
      <FormControl mb={6}>
        <Select
          value={selectedVideo}
          onChange={handleSelectVideo}
          onInputChange={handleInputChange}
          options={videos}
          formatOptionLabel={getOptionLabel}
          styles={colourStyles}
          isClearable
          isSearchable
          placeholder="Rechercher une vidéo..."
        />
      </FormControl>
      {selectedVideo && (
        <Box mb={6}>
          <iframe
            width="100%"
            height="315"
            src={`https://www.youtube.com/embed/${selectedVideo.videoId}`}
            title="YouTube video player"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
          <Box marginTop={8}>
            <ThemeList onSelectedThemesChange={handleThemesChange} />
          </Box>
          <Box mt={6}>
            <Button
              onClick={handleUploadVideo}
              bgColor={theme.colors[colorMode].bgRed}
              _hover={{ bgColor: theme.colors[colorMode].bgRedHover }}
              width="100%"
              mt={2}
            >
              Télécharger la vidéo
            </Button>
            {loading && <Spinner />}
            {successMessage && (
              <Box
                color={theme.colors[colorMode].txtGreen}
                mt={4}
                textAlign={"center"}
              >
                {successMessage}
              </Box>
            )}
          </Box>
        </Box>
      )}
      {error && (
        <Box color={theme.colors[colorMode].txtRed} mt={4} textAlign={"center"}>
          {error}
        </Box>
      )}
    </>
  );
}
