import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  useColorMode,
  useTheme,
  ModalFooter,
} from "@chakra-ui/react";
import GameCountdown from "./GameCountdown";

export default function ModalGame({
  isOpen,
  onClose,
  title,
  footer,
  countdown,
  children,
}) {
  const theme = useTheme();
  const colorMode = useColorMode().colorMode;

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      isCentered={true}
      alignItems="center"
      width="100%"
      closeOnOverlayClick={false}
    >
      <ModalOverlay bgColor={theme.colors[colorMode].modalOverlay} />
      <ModalContent
        justifyContent="center"
        alignItems="center"
        bg="transparent"
        boxShadow="none"
      >
        {title && (
          <ModalHeader
            width="100%"
            textAlign="center"
            fontSize={{ base: "2xl", md: "3xl", lg: "4xl" }}
          >
            {title}
          </ModalHeader>
        )}
        <ModalBody width="100%">{children}</ModalBody>
        {footer && <ModalFooter width="100%">{footer}</ModalFooter>}
        {countdown && countdown >= 0 && (
          <GameCountdown countdown={countdown} size={3} />
        )}
      </ModalContent>
    </Modal>
  );
}
