import { Route, Routes } from "react-router-dom";

import Layout from "./pages/Layout";
import HomePage from "./pages/HomePage";
import SongsPage from "./pages/SongsPage";
import SongFormPage from "./pages/SongFormPage";
import LoginPage from "./pages/LoginPage";
import GamePage from "./pages/GamePage";
import ErrorPage from "./pages/ErrorPage";

export default function App() {
  return (
    <Routes>
      <Route element={<Layout />}>
        <Route element={<HomePage />} path="/" />
        <Route element={<SongsPage />} path="/songs" />
        <Route element={<SongFormPage />} path="/new" />
      </Route>
      <Route element={<LoginPage />} path="/login" />
      <Route element={<GamePage />} path="/game/:id" />
      <Route path="*" element={<ErrorPage />} />
    </Routes>
  );
}
